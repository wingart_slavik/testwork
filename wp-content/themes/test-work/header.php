<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php bloginfo('name'); ?></title>
    <meta name = "format-detection" content = "telephone=no" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <meta name="description" content="<?php bloginfo('description') ?>">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <?php wp_head(); ?>

    <script>
    	var templateUrl = "<?php echo get_template_directory_uri(); ?>"
    </script>

</head>

<body <?php body_class(); ?>>
    <!--==============================header=================================-->
      <header>
        <?php 
            $headerLogo = get_field('header_logo', 'options');
            $headerPhone = get_field('header_phone', 'options');
        ?>
        <div class="container">
          
          <div class="clearfix">

            <?php if ($headerLogo): ?>            
                <div class="logo"><a href="/"><img src="<?php echo $headerLogo; ?>"></a></div>
            <?php endif ?>
            
            <?php if ($headerPhone): ?>
                <div class="header__phone">
                   <?php echo $headerPhone; ?>
                </div>
            <?php endif ?>

          </div>

        </div>
      </header>
    <!--========================== !!! Header END !!!=========================-->    

    <div class="nav-slider-wrap">
      <!--========================== !!! Navigation !!!=========================-->
      <nav>
        <div class="container">
          <button id="resp-toggle-btn" class="resp-toggle-btn">
            <span class="line1"></span>
            <span class="line2"></span>
            <span class="line3"></span>
          </button>
          
          <div class="clearfix">
            
            <?php 
                wp_nav_menu(array(
                  'theme_location' => 'header_menu',
                  'menu' => 'primary', // название меню
                  'menu_class' => 'menu', // класс для меню
                  'menu_id' => 'primary_nav', // id для меню
                ));
            ?>
            
            <?php get_search_form(); ?>

          </div>

        </div>
      </nav>
    <!--========================== !!! Navigation END !!!=========================-->
    

        <?php $args = array(
            'posts_per_page'   => 3,
            'orderby'          => 'date',
            'order'            => 'ASC',
            'post_type'        => 'slides',
            'post_status'      => 'publish'
        );
        $posts_array = get_posts( $args ); ?>

        <?php if ($posts_array): ?>
            <!--========================== !!! Slider !!!=========================-->
              <section class="slider-section">
                <div class="container">
                  
                  <div class="slider">
                    <!-- slider items -->
                    <div class="swiper-wrapper">
                    
                    <?php foreach ($posts_array as $post): ?>
                    <?php setup_postdata($post); ?>
                      <!-- slider item -->
                      <div class="swiper-slide">
                        <figure class="slider__item">
                          <figcaption class="slider__item-description">
                            
                            <?php the_title(); ?>

                            <div class="slider__item-info">
                              <time class="slider__item-time"><span><?php the_time('j F, Y'); ?></span></time>  
                              <span class="btn">Новости</span>
                            </div>


                          </figcaption>
                          <div class="slider__item-img" style="background: url(<?php the_post_thumbnail_url('slide-img'); ?>) 50% 0 no-repeat; background-size: cover;"></div>
                        </figure> 
                      </div>
                      <!-- slider item END-->
                    <?php endforeach ?>

                    <?php wp_reset_postdata(); ?>

                    </div>
                    <!-- slider items END-->

                    <!-- slider buttons -->
                    <div class="slider__buttons">
                      <button class="slider-button slider-button--prev"></button>
                      <button class="slider-button slider-button--next"></button>
                    </div>
                    <!-- slider buttons END-->

                  </div>

                </div>
              </section>
            <!--========================== !!! Slider END !!!=========================-->
        <?php endif ?>
    </div>
    