<?php

	
	function my_theme_setup(){
		add_theme_support( 'post-thumbnails' );
	}
	add_action( 'after_setup_theme', 'my_theme_setup' );

	// custom image sizes
	add_image_size( 'slide-img', 760, 336 );


	function theme_files_include(){  
		// main style
		wp_enqueue_style( 'main-style', get_template_directory_uri() . '/css/main.css', array(), '1.0' );
		
		//Include js 
		wp_deregister_script('jquery');
		wp_deregister_script('jquery-migrate');

	    wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery-2.1.3.min.js' );
	    wp_enqueue_script( 'jquery' );
	    
	    wp_register_script( 'jquery-migrate', get_template_directory_uri() . '/js/jquery-migrate-1.2.1.min.js' );
	    wp_enqueue_script( 'jquery-migrate' );

	    wp_register_script( 'wingart-custom-js', get_template_directory_uri() . '/js/script.js', false, false, true );
	    wp_enqueue_script( 'wingart-custom-js' );
	    
	}

	add_action( 'wp_enqueue_scripts', 'theme_files_include' );


	// reg navigation
	register_nav_menus(array(
	  'header_menu' => 'Menu for header'
	));

	// register custom post types

	function create_post_type() {
		// custom post type for process system

		register_post_type( 'slides',
			array( 
				'labels' => array( 
				'name' => __( 'Слайды' ),
				'singular_name' => __( 'Слайд' ),
				'add_new' => __('Добавить новый'),
				'add_new_item' => __('Добавить слайд'),
				'edit_item' => __('Редактировать слайд'),
				'new_item' => __('Новый слайд'),
				'all_items' => __('Все слайды'),
				'view_item' => __('Смотреть слайд'),
				'search_items' => __('Найти слайд'),
				'not_found' => __('Ничег не найдено'),
				'not_found_in_trash' => __('Ничег не найдено'), 
				'menu_name' => 'Слайдер'
			), 
			'public' => true,
			'publicly_queryable' => true,
			'menu_position' => 10,
			'menu_icon' => 'dashicons-id', 
			'rewrite' => array('slug' => 'slides'),
			'supports' => array('title', 'editor', 'thumbnail', 'revisions')
			) 
		);
	}

	add_action( 'init', 'create_post_type' );

	// // Option page ACF
	if( function_exists('acf_add_options_page') ) {
		
		acf_add_options_page(array(
			'page_title' 	=> 'Глобальные настройки',
			'menu_slug' 	=> 'global-settings',
			'menu_title'	=> 'Глобальные настройки',
			'capability'	=> 'edit_posts',
			'icon_url' 		=> 'dashicons-welcome-widgets-menus',
	        'position' 		=> 10
			//'redirect'		=> false
		));
	}
	// // Option page ACF !! END !!


	// // Excerpt lengh
	function custom_excerpt_length( $length ) {
		return 15;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


	// // Custom shortcodes

	function helloWorlFunc()
	{
		return 'Hello World my name is Slava';
	}

	add_shortcode( 'helloWorld' , 'helloWorlFunc');




?>