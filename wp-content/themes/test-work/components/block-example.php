<?php 
    $contentLeftCol = get_sub_field('blue_box_with_2_columns_left_col');
    $contentRightCol = get_sub_field('blue_box_with_2_columns_right_col');
?>

<article class="pad_box4 serv_box">
    <div class="container">
        <div class="row">

            <div class="col-lg-12">
                <div class="accent_bg2 clearfix pad_box3">
                    <?php if ($contentLeftCol): ?>
                        <div class="serv_operation serv_left matchheight" data-mh="block-blue_box_with_2_columns">
                            <?php echo $contentLeftCol; ?>
                        </div>
                    <?php endif ?>
                    
                    <?php if ($contentRightCol): ?>
                        <div class="serv_operation line serv_right matchheight" data-mh="block-blue_box_with_2_columns">
                            <?php echo $contentRightCol; ?>
                            <!-- <div class="expand"><span>Expand</span></div> -->
                        </div>
                    <?php endif ?>
                </div>
            </div>

        </div>
    </div>
</article>