<?php 
	/*
		Template Name: Главная страница
	*/
?>
<?php get_header(); ?>
	
	<!--==============================content================================-->
	<section class="primary-content">
	  <div class="container">
	    
	    <div class="row">
	      <div class="col-md-4">

	      	<?php 
	      		$sidebarTitle = get_field('sidebar_title');
	      	?>

	      	<?php if ($sidebarTitle): ?>
	        	<h2 class="title-2"><?php echo $sidebarTitle; ?></h2>
	      	<?php endif ?>
	        

	        <div class="about-tether">
	          <!-- Start the Loop. -->
			 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			 <?php endwhile; endif; ?>
			<!-- !!! content END !!! -->
	        </div>

	      </div>

	      	<?php 
	      		$term = get_field('home-posts');
	      	?>
			
			<?php if ($term): ?>
	      		
		    <div class="col-md-8">
		        
		        <h2 class="title-2"><?php echo $term->name; ?></h2>

		        <?php 
			        $args = array(
			        	'posts_per_page'   => 3,
			        	'category'         => $term->term_id,
			        	'post_status'      => 'publish',
			        	'suppress_filters' => true 
			        );
			        $posts_array = get_posts( $args );
		        ?>
				
				<?php if ($posts_array): ?>
					<ul class="news-list">
						<?php foreach ($posts_array as $post): ?>
							<?php setup_postdata($post); ?>
							<li class="news-list__item">
					            <div class="news-list__item-thumbnail">
					              <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
					            </div>
					            <div class="news-list__item-excerpt">
					              <time><?php the_time('j F, Y'); ?></time>
					              <?php the_excerpt(); ?>
					            </div>
					        </li>
						<?php endforeach ?>
			        </ul>

			        <?php wp_reset_postdata(); ?>
				<?php endif ?>	

		    </div>
		    <?php endif ?>
	    </div>

	  </div>
	</section>

	

<?php get_footer(); ?>