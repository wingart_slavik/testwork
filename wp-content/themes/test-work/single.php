<?php get_header(); ?>

<section class="primary-content">
	<div class="container">
		
		<!-- Start the Loop. -->
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<h1 class="title-2"><?php the_title(); ?></h1>

			<?php the_content(); ?>

		<?php endwhile; endif; ?>
		<!-- !!! content END !!! -->

	</div>
</section>

<?php get_footer(); ?>
