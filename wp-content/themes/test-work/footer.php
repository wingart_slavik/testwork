<!--==============================footer=================================-->
<footer>
	<?php 
		$footerLogo = get_field('footer_logo', 'options');
	?>
  	<div class="container">
  		<?php if ($footerLogo): ?>
    		<a href="/"><img src="<?php echo $footerLogo; ?>"></a>
  		<?php endif ?>
  	</div>
</footer>


    <?php wp_footer(); ?>

</body>
</html>
