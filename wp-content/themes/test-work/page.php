<?php get_header(); ?>

<article id="content1">
	<!-- Start the Loop. -->
	 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		

		<?php if(have_rows('content_blocks')): ?>
			
			<?php while(have_rows('content_blocks')):the_row(); ?>
				
				<?php if (get_row_layout() == 'main_image_box'): ?>
				<!-- main img box -->	
					<?php get_template_part('components/block', 'main_image_box'); ?>

				<!-- !!! -->
				<?php endif ?>


			<?php endwhile; ?>

		<?php endif ?>
		
		
	 <?php endwhile; endif; ?>
	<!-- !!! content END !!! -->
</article>
<?php get_footer(); ?>
