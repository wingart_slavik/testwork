<?php get_header(); ?>
	
	<div class="container">
		<section class="error-404 not-found">
			
			<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.'); ?></h1>
			
			<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?' ); ?></p>

			<?php get_search_form(); ?>

		</section><!-- .error-404 -->		
	</div>

<?php get_footer(); ?>
