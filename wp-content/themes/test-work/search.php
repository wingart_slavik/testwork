<?php get_header(); ?>

	<section class="primary-content">
		
		<div class="container">
			<!-- Start the Loop. -->
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<?php the_title(); ?>

			<?php endwhile; endif; ?>
			<!-- !!! content END !!! -->
		</div>

	</section><!-- .content-area -->

<?php get_footer(); ?>
