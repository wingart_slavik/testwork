
var tabsBox 	= $(".tabs"),
	carousel 	= $('.carousel'),
	slider   	= $(".slider");

if(tabsBox.length){
  include("js/easyResponsiveTabs.js");
}

if (slider.length){
	include("js/swiper.jquery.js");
}

function include(url){ 
  document.write('<script src="'+templateUrl+'/'+ url + '"></script>'); 
}


$(document).ready(function(){

	if (slider.length){
		var swiper = new Swiper( slider, {
	        pagination: false,
	        paginationClickable: false,
	        nextButton: '.slider-button--next',
	        prevButton: '.slider-button--prev'
	    });
	}
	
	// responsive navigation toggle button
	$("#resp-toggle-btn").on('click', function(){
	  	$("body").toggleClass('navTrue');
	})

	$(".menu").on('click', 'a', function() {
		$('body').removeClass('navTrue');
	})

	// init of tabs
	if (tabsBox.length){
		tabsBox.each(function(){
			var currentTabs = $(this);

			currentTabs.easyResponsiveTabs();
		})
	}

	// init of carousel
  	if (carousel.length){
	  	carousel.each(function(){
	  		var currentCarousel = $(this),
	  			items 			= currentCarousel.data('items'),
	  			singleItem      = false,
	  			itemsDesc 		= currentCarousel.data('items-desc'),
	  			itemsDescSm		= currentCarousel.data('items-desc-sm'),
	  			itemsTab 		= currentCarousel.data('items-tab');

	  		if (items < 1){
	  			return false;
	  		}

	  		if (items == 1){
	  			singleItem = 1;
	  			itemsDesc = 1;
		  			itemsDescSm = 1;
	  			itemsTab = 1;
	  		}

	  		currentCarousel.owlCarousel({
	  			items : items,
	  			singleItem : singleItem,
	  			itemsDesktop : [1199, itemsDesc],
		        itemsDesktopSmall : [979, itemsDescSm],
		        itemsTablet : [768, itemsTab],
		        itemsTabletSmall : false,
		        itemsMobile : [479, 1],
		        navigation : true,
		        navigationText : [" ", " "],
		        transitionStyle : "fadeUp",
		        autoHeight : true
	  		})
	  	})
	}




})