
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="search" class="search-form__input" placeholder="Поиск" value="<?php echo get_search_query(); ?>" name="s" />
</form>
