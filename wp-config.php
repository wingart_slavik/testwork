<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kiwi_testwork');

/** MySQL database username */
define('DB_USER', 'kiwi_testwork');

/** MySQL database password */
define('DB_PASSWORD', 'testwork123');

/** MySQL hostname */
define('DB_HOST', 'kiwi.mysql.tools');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'U[l7u`Of>-&J<=V)BH6.&6(>dB);HFOJ}$0[Dm@QaP,(jI{;HpA?}Ga*` .).V$v');
define('SECURE_AUTH_KEY',  ':}(5=df~D=`O?YCmY_=q?M&pv[iyC(?aLk 0]*lUXs>X.%(obJw2%1C.2-(2v)Lo');
define('LOGGED_IN_KEY',    'fbJ5e2 <VMZxN+hiFzGeMIVh0k~[20N8Gcb3&XNdkp,Gci0^NWSvsCkGWi:TGW2B');
define('NONCE_KEY',        ' *16!~}HZq8{R`lN9-X+-dA;:-?;-!fw[AZOA^V3f6dZ6v(9u4?Z#pa!Jw~/-=YI');
define('AUTH_SALT',        '#L@BA)T$ezp|lGW>+qk4AMtCDx8~jC%&T2ST%OH4u5$R@8ZIbziXs{uB6B@V~Zl ');
define('SECURE_AUTH_SALT', '3bQ,0ZvhgVngU>K`:MyMT*Ys?! WoU%.ZWJbo5Icy)KUnM5wU(D6 uj:^<~el`.C');
define('LOGGED_IN_SALT',   'HObz>r j@AeS0bXr`06B /.#:H.`fRrK4LiM@C>Gn9URn0XJNRV@$0]ILNtd$0X5');
define('NONCE_SALT',       '73*+j_q4z3.moX!0A4)B,sx,CF7 ~~LNMTh=6on(B=zco?LtYW)zdvPa=GF*[vd:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tw_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
